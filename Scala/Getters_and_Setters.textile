h1. Getters and Setters

- Categories := Scala

- Tags := get, set

[[More Info|http://dustinmartin.net/getters-and-setters-in-scala/]]

```java
class Person() {
	private var _age = 0
	private var _name = ""

	def age = _age
	def name = _name

	def age_= (value:Int):Unit = _age = value
	def name_= (value:String):Unit = {
		assert(value.length() > 5, "Name is too short")
		_name = value
	}
}

var person = new Persion()
person.age = 20
person.name_= "Wooster"
```

