#!/usr/bin/env perl

# Generates the Home.textile page by traversing snippets directory
#
# Usage: makeHome.pl snippets Home.textile
#
# Author: Roman Briskine
#

use strict;
use warnings;
use File::Basename;
use Data::Dumper;

if (@ARGV != 3) {
	print "Usage: makeHome.pl snippets categories Home.textile\n";
	exit(1);
}

sub buildCatPage {
	my ($catName, $snippets, $dirCats) = @_;
	(my $fnCat = $catName) =~ s/[^A-z0-9]+/_/g;
	open(FCAT, ">", "$dirCats/${fn}.textile");
	print FCAT 

my ($dirSnippets, $dirCats, $pathOut) = @ARGV;

my %categories;

for my $i (glob("$dirSnippets/*.textile")) {
	my ($fn, $path, $sfx) = fileparse($i);
	my ($catName, $snippetName);
	open(FSNP, "<", $i) or die("Failed to read the snippet [$i]." . $!);
	while (<FSNP>) {
		chomp;
		if ( ! $snippetName && /^h1\./ ) {
			$_ =~ /^h1\. (.+)$/;
			$snippetName = $1;
		}
		if ( ! $catName && /^- Categories/ ) {
			if ( ! $snippetName ) {
				die("Bad file format [$i]: categories come before h1");
			}
			$_ =~ s/^- Categories := //;
			my @snippetCats = split(/, /, $_);
			for my $sc (@snippetCats) {
				$categories{$sc} = [] unless defined $categories{$sc};
				push(@{$categories{$sc}}, $snippetName);
			}
		}
		last if ($snippetName && $catName);
	}
	close(FSNP);
}

#print Dumper(%categories);
open(FOUT, ">", $pathOut);
print FOUT "h1. Code snippets\n";

for my $cat ( sort( keys(%categories) ) ) {
	print FOUT "\n\n";
	print FOUT "h2. $cat\n";
	print FOUT "\n";
	for my $snip ( sort(@{$categories{$cat}}) ) {
		(my $snipPath = $snip) =~ s/[^A-z0-9]+/_/g;
		print FOUT "# [[$snip|$snipPath]]\n";
	}
}

close(FOUT);

