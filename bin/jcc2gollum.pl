#!/usr/bin/env perl

# Converts jcc database (exported into XML file) to gollum wiki
#
# Usage: jcc2gollum.pl jccdb.xml dirout
#
# Author: Roman Briskine

use strict;
use warnings;
use XML::Simple;
use Data::Dumper;

if (@ARGV != 2) {
	print "Usage: jcc2gollum.pl jccdb.xml dirout\n";
	exit(1);
}

my ($pathIn, $dirOut) = @ARGV;

# Maps syntax terms from JCC to Rouge
my %syntaxMap = (
		'unix shell script' => 'shell',
		'c++' => 'cpp',
		'java' => 'java',
		'perl' => 'perl',
		'latex' => 'tex',
		'sql' => 'sql',
		'php' => 'php',
		'ruby' => 'ruby',
		'sas' => 'r',
		'makefile' => 'make',
	);

# Maps categories (to standardize capitalisation)
my %catMap = (
		'bash' => 'Bash',
		'bioinfo' => 'BioInfo',
		'c++' => 'Cpp',
		'eclipse' => 'eclipse',
		'gatk' => 'GATK',
		'latex' => 'LaTeX',
		'linux' => 'Linux',
		'macports' => 'MacPorts',
		'mysql' => 'SQL',
		'os x' => 'MacOS',
		'php' => 'PHP',
		'perl' => 'Perl',
		'r' => 'R',
		'ruby' => 'Ruby',
		'ruby on rails' => 'RoR',
		'scala' => 'Scala',
		'util' => 'Util',
		'git' => 'Git',
		'gnumake' => 'GnuMake',
	);
	

# Setting SuppressEmpty to undef will return empty elements as undef rather than an empty hash
# Tag may have multiple values. If array is not forces, single values are returned as a string.
my $data = XMLin($pathIn, SuppressEmpty => undef, ForceArray => ['tag']);
#print Dumper($data);
#die("Oah noah");


my $snippets = $data->{'snippet'};
while (my ($name, $s) = each(%$snippets)) {
	my $cat = lc($s->{'category'});

	if (defined($catMap{$cat})) {
		$cat = $catMap{$cat};
	} else {
		print "Category ($cat) is not mapped. The original will be copied.\n";
		$cat = $s->{'category'};
	}

	my $syntax = "";
	if ($cat eq 'R') {
		$syntax = 'r';
	} elsif ( defined($s->{'syntax'}) && defined( $syntaxMap{ lc($s->{'syntax'}) } ) ) {
		$syntax = $syntaxMap{ lc($s->{'syntax'}) };
	}

	(my $fName = $name) =~ s/[^A-z0-9]+/_/g;
	my $pathOut = "$dirOut/$fName" . ".textile";
	open(my $fh, ">", $pathOut) or die("Unable to write to $pathOut. $!");
	binmode($fh, ':utf8');
	print $fh "h1. $name\n";
	print $fh "\n";
	print $fh "- Categories := $cat\n";
	print $fh "\n";
	print $fh "- Tags := " . join(", ", @{$s->{'tag'}}) . "\n";
	print $fh "\n";
	print $fh "```$syntax\n";
	print $fh $s->{'code'} . "\n";
	print $fh "```\n";
	print $fh "\n";
	if ( defined($s->{'comment'}) ) {
		print $fh $s->{'comment'}, "\n";
		print $fh "\n";
	}
	close($fh);
}


#print join(",", keys(%{$data->{'snippet'}})), "\n";
#print scalar({$data->{'snippet'}}), "\n";
#my $cnt = 0;
#for my $snippet (@{$data->{'jcc-snippets-package'}}) {
#	$cnt++;
#}
#print "SnippetN: $cnt\n";

