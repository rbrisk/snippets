#!/usr/bin/env perl

# Moves snippets into category-based directories
#
# Usage: snippetsToCatDirs.pl srcDir tgtDir
#
# Author: Roman Briskine

use strict;
use warnings;
use File::Basename;
use File::Path qw(make_path);
use File::Spec;

if (@ARGV != 2) {
	print "Usage: snippetsToCatDirs srcDir tgtDir\n";
	exit(1);
}

my ($dirSrc, $dirTgt) = @ARGV;
my $unsafePtrn = qr/[^A-z0-9]+/;
my $safeChar = "_";

for my $i (glob("$dirSrc/*.textile")) {
	my ($fn, $path, $sfx) = fileparse($i);
	my $catName;
	open(FSNP, "<", $i) or die("Failed to read the snippet [$i]. $!");
	while (<FSNP>) {
		chomp;
		if (/^- Categories := (.+)$/) {
			$catName = $1;
			last;
		}
	}
	close(FSNP);
	if (!$catName) {
		print "ERROR: bad file format [$i]. Category record is absent or invalid.\n";
	}
	(my $catDirName = $catName) =~ s/$unsafePtrn/$safeChar/g;
	my $dirCat = File::Spec->join($dirTgt, $catDirName);
	if ( ! -d $dirCat) {
		make_path($dirCat);
		#print "$dirCat", "\n";
	}
	my $pathTgt = File::Spec->join($dirCat, $fn);
	system("git mv $i $pathTgt");
	#print "git mv $i $pathTgt", "\n";
}


