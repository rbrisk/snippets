h1. Modules

- Categories := Sysadmin

- Tags := module, environment, packages, installations, software


h2. CLASSPATH

Note that setting CLASSPATH is probably a bad way to handle java libraries. Many libraries have their own dependencies installed along with the main jar. Those dependencies my conflict with each other when multiple libraries are loaded. That being said...

Modules application expands asterisks used within arguments for @prepent-path@ or @append-path@. Therefore, adding a directory with jar files using asterisk is impossible. You have to add all files one by one.

```tcl
foreach {jarFile} [ glob $prefix/*.jar ] {
	prepend-path CLASSPATH $jarFile
}
```


h2. Resetting LC_ALL

It is very hard to cache an environment variable value (see below). The recommended way would be to have a separate module and make it a prerequisite. Loading it automagically is a bad idea because multiple modules may depend on it. See Prerequisites.

```tcl
if { [module-info mode load ] && ! [ is-loaded "localeC" ] } {
	puts stderr "[ module-info name ] requires localeC module to be loaded first."
	exit
}
```

Hard way to set change @LC_ALL@. Note that on unload @setenv@ becomes @unsetenv@ while @unsetenv@ becomes @setenv@. Hence, the directives appear the opposite to what is expected.

```tcl
set cachedLC [ module-info name ]
regsub -all "/" $cachedLC "_" cachedLC
set cachedLC ${cachedLC}_cached_LC_ALL

if [ module-info mode load ] {
	if [ info exists ::env(LC_ALL) ] {
		setenv $cachedLC $::env(LC_ALL)
	}
	setenv LC_ALL C
}
if [ module-info mode remove ] {
	if [ info exists ::env($cachedLC) ] {
		unsetenv LC_ALL $::env($cachedLC)
		setenv $cachedLC ""
	}
}
```


h2. Prerequisites

Fail if the prerequisite is not loaded

```tcl
set prereqs { util/localeC seq/picard }
if { [ module-info mode load ] } {
	foreach { p } $prereqs {
		if { ! [ is-loaded $p ] } {
			puts stderr "Please load $p before loading [module-info name]\n"
			puts stderr "All prerequisites: $prereqs\n"
			exit
		}
	}
}
```


h2. Non-module prerequisites

Check and enforce certain version, e.g. python v2.x. The command to set the shell's version does not work for some reason.

```tcl
regsub -- {^Python (\d+\..+)$} [exec python --version 2>@1] {\1} pver
if { regexp {^2\.} $pver } {
	[ pyenv shell 2.7.8 ]
}
```
