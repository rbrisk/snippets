h1. Extract single entry from fasta

- Categories := Bio

- Tags := extract, fasta, processing

No printing unless in the range specified by the two patterns, then pipe it to remove the last line. (The range is inclusive.)

```shell
sed -n '/^>SeqId/,/^>/ p' seqs.fa | sed '$d'
```

If sequences are numbered, it may be necessary to add a space or EOL.

```shell
sed -n '/^>Scaffold25\s/,/^>/ p' seqs.fa | sed '$d'
```

