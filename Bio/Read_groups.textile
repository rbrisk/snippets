h1. Read groups

- Categories := Bio

- Tags := GATK, RG, bwa, read group, sample

@-R@ adds the read group information required by GATK

```shell
id=Alyr_200
bwa mem -M -t 8 \
	-R "@RG\tID:${id}_01\tPL:Illumina\tPU:${id}_01\tLB:${id}_01\tSM:${id}_01" \
	ref.fa r1.fastq.gz r2.fastq.gz | samtools view -Su - | samtools sort \
	-m 30000000000 - fileOut
```

If you forget to add the read groups while running bwa mem, you can add them afterwords using Picard

```shell
java -Xmx2g -jar $rlib/picard-tools/AddOrReplaceReadGroups.jar \
	I=file.bam O=file_rg.bam RGID=Alyr_200_01 RGLB=Alyr_200_01 RGPL=Illumina \
	RGPU=Alyr_200_01 RGSM=Alyr_200_01
```

Check RG

```shell
samtools view -H file_rg.bam | grep RG
```

