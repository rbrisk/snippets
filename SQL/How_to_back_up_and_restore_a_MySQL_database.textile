h1. How to back up and restore a MySQL database

- Categories := SQL

- Tags := backup, database, mysql, sql

Back up from the command line (using mysqldump).

```shell
mysqldump --opt -u [uname] -p[pass] [dbname] > [backupfile.sql]
``` 

Back up the MySQL database.

```shell
mysqldump -u [uname] -p[pass] [dbname] | gzip -9 > [backupfile.sql.gz]
```
 
Restoring the MySQL database.

```shell
mysql -u [uname] -p[pass] [db_to_restore] < [backupfile.sql]
```

*Warning*: using @-p[pass]@ will log the password to @.bash_history@.

