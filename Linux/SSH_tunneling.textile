h1. SSH tunneling

- Categories := Linux

- Tags := MySQL, SSH, tunneling

[[More info|http://chxo.com/be2/20040511_5667.html]]

* @-f@ sends into the background
* @-N@ prevents remote command execution
* @-L@ port forwarding (@local_port:host:remote_port@)

```shell
ssh -fNg -L 3307:localhost:9906 briskin2@db2.msi.umn.edu
```

