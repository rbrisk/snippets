h1. OpenSSH and proxies

- Categories := Linux

- Tags := gateway, proxy, ssh, tunnel

[[More Info|http://en.wikibooks.org/wiki/OpenSSH/Cookbook/Proxies_and_Jump_Hosts]]

Sample configuration

```shell
Host f47
	User username
	HostName fgcz-c-047.fgcz-net.unizh.ch
	IdentityFile $HOME/.ssh/id_rsa
	ProxyCommand ssh username@fgcz-transfer.uzh.ch -W %h:%p
```

