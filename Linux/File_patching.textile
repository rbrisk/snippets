h1. File patching

- Categories := Linux

- Tags := diff, patch

```shell
cp -a pkg/file.pm{,.orig}
diff -u pkg/file.pm.orig pkg/file.pm > pkg_file.pm.patch
patch pkg/file.pm < pkg_file.pm.patch
```

