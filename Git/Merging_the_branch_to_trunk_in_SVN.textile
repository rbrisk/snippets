h1. Merging the branch to trunk in SVN

- Categories := Git

- Tags := branch, merge, svn, trunk

```shell
# Update your branch
svn update

# Commit all your changes in the branch
svn commit

# Find when the branch was cut or check the trunk's latest revision
svn log --stop-on-copy
# -OR- (from the trunk directory)
svn info

# Merge the branch. You can also switch the working directory but I haven't tested it
cd trunk
svn update
svn merge -r XXX:HEAD svn+ssh://localhost/svnrepos/branches/branch1
svn commit -m 'Merged branch1 into trunk'
```

