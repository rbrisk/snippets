h1. External Disk Mounting

- Categories := MacOS

- Tags := disk, external, mount

Mountain Lion doesn't mount external drives unless a user is logged in.

To see the list of all available devices.

```shell
diskutil list
```

To mount a drive with a partitioning scheme.

```shell
diskutil mountDisk /dev/disk5
```

