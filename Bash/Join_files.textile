h1. Join files

- Categories := Bash

- Tags := column, file, join, zip

```shell
paste -d "\t" a.txt b.txt
paste -d "   " a.txt b.txt
```

