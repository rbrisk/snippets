h1. Tabs

- Categories := Bash

- Tags := variable, command, cmd, optional, default


h2. Default value

```shell
val1=$(val2:-defaultstr)
```


h2. Commands in a variable

It is [[discouraged|http://mywiki.wooledge.org/BashFAQ/050]] to place commands in a variable. Variables hold data, code should be stored in functions. To pass options at runtime, you can use @eval@ with parameter expansion or field splitting.

```shell
eval ping -q "${count:+'-c' \"\$count\"}" '"$HOSTNAME"'

oIFS=$IFS IFS=' '
ping -q ${count:+'-c' "$count"} "$HOSTNAME"
IFS=$oIFS; unset -v oIFS
```


h2. Capitalisation

[[By convention|http://stackoverflow.com/questions/673055/correct-bash-and-shell-script-variable-capitalization]], only environmental variables (PAGER, EDITOR, ...) and internal shell variables (SHELL, BASH_VERSION, ...) are capitalised. It means that only exported variables need to be capitalised.


