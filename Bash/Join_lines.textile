h1. Join lines

- Categories := Bash

- Tags := join, line, paste

[[More Info|http://stackoverflow.com/questions/2764051/joining-multiple-lines-into-one-with-bash]]

```shell
tail +10 -20 aaa.txt | paste -sd "" -
```

@tr@ can only process data from @stdin@.

```shell
cat aaa.txt | tr -d '\n'
```

