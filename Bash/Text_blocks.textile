h1. Text blocks

- Categories := Bash

- Tags := heresay, multiline, string, text

```shell
echo "
multiline
string
"
```

Variable assignment. Line breaks will be lost.

```shell
i=1

read -d '' varName <<EOB
line $i
line 2
EOB
```

Expand variables

```shell
cat > out.txt <<EOB
line $i
line 2
EOB
```

Escape variables

```shell
cat > out.txt <<'EOB'
line $i
line 2
EOB
```

