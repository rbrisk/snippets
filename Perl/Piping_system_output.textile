h1. Piping system output

- Categories := Perl

- Tags := piping, system

h2. Option 1

```perl
my $cmd = "cat dog.txt";
open(PIPE, "$cmd |");
while (<PIPE>) {
	print $_; 
}
```

To include @STDERR@. Note that @|&@ will not work. [[More info|http://perldoc.perl.org/perlfaq8.html#How-can-I-capture-STDERR-from-an-external-command?]]

```perl
open(PIPE, "$cmd 2>&1 |");
```


h2. Option 2

*Careful*: semicolon at the end of the block is required. Otherwise, cryptic errors will ensue

```perl
use Capture::Tiny qw( capture );

my ($stdout, $stderr, $exit) = capture {
	system($cmd);
};
```

