h1. Timing the code in Perl

- Categories := Perl

- Tags := Ticktock, benchmark, timing

```perl
use Time::HiRes qw( clock );

clock();

for (my $i = 0; $i < $maxSize; $i++) {
	# Do something
}

print clock(), "\n";
```

