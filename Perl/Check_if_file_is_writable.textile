h1. Check if file is writable

- Categories := Perl

- Tags := write, mkdir, writable, file, IO

h2. Simple check

```perl
die("Unable to write to ($path)") unless -w $path
```

h2. Argument check

We need to make sure that the parent directory exists.

```perl
use File::Basename;
use File::Path qw( make_path );

if ($path) {
	my $dir = basename($path);
	make_path($dir) unless -d $dir;
	die("Unable to write to ($path)") unless -w $dir;
}
```

