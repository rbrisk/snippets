h1. Hash from array

- Categories := Perl

- Tags := array, dictionary, hash

```perl
%hash = map { $_ => 1 } @arr;

%hash = ();
while (my ($k, $v) = each(@arr)) {
	$hash{$k} = $v;
}
```

