h1. Join file path

- Categories := Perl

- Tags := File::Spec, path

```perl
use File::Spec;
my $fPath = File::Spec->catfile($dirIn, $fName);

use File::Spec::Functions;
my $pathOut = catfile($dirOut, $fName);
```

