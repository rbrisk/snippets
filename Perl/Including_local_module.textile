h1. Including local module

- Categories := Perl

- Tags := include, module, script directory

Assuming the local module is in the lib directory relative to the script being executed.

```perl
use File::Basename;
use lib File::Spec->join(dirname($0), "lib");
use LocalModule;
```

