h1. PDF splitting and joining

- Categories := Util

- Tags := PDF, ghostscript, gs, join, split

[[Split a file|http://www.commandlinefu.com/commands/view/4823/split-a-multi-page-pdf-into-separate-files]]

```shell
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=1 -dLastPage=5 -sOutputFile=Part1.pdf input.pdf
```

[[Join files|http://www.linuxjournal.com/content/tech-tip-using-ghostscript-convert-and-combine-files]]

```shell
gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile=combined.pdf part1.pdf part2.pdf part3.pdf
```

