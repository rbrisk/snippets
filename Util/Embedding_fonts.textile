h1. Embedding fonts

- Categories := Util

- Tags := embed, font, ghostscript, pdf

R does not usually embed fonts when @pdf@ command is issued but fonts can be subsequently embedded with @ghostscript@ if necessary.

Find out which fonts are used and which are embedded.

```shell
pdffonts input.pdf
```

Embed fonts with @ghostscript@. @-dUseCIEColor@ is required due to a bug.

```shell
gs -dSAFER -dNOPLATFONTS -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dUseCIEColor \
	-dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer \
	-dMaxSubsetPct=100 -dSubsetFonts=true -dEmbedAllFonts=true \
	-sOutputFile=input.pdf -f output.pdf
```

One line
```shell
gs -dSAFER -dNOPLATFONTS -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dUseCIEColor -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer -dMaxSubsetPct=100 -dSubsetFonts=true -dEmbedAllFonts=true -sOutputFile=input.pdf -f output.pdf
```

