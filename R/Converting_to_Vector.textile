h1. Converting to Vector

- Categories := R

- Tags := data.frame, matrix, vector

Depends on the type of object.

Matrix

```r
v <- as.vector(m)
```

data.frame

```r
v <- unlist(df)
```

