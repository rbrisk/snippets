h1. ggplot Variable names in aes

- Categories := R

- Tags := aes, ggplot, variables

```r
# aes_string can be used for dynamic values
orientCol <- "OrientCoarse"
gObj <- ggplot(dfProp, aes_string(x=orientCol, y="BothProp")) + 
		geom_bar(aes(fill=variable), stat="identity", position="dodge") +
		scale_fill_manual(labels=fillLabels, values=cbcPalette, name="") +
		scale_x_discrete(name="Orientation", limits=xLimits) + 
		ylab("Proportion")
```

