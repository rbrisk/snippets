h1. Number Formatting

- Categories := R

- Tags := formatting, pretty, sprintf

```r
> prettyNum(123456789, big.mark=",")
[1] "123,456,789"


# Another way is to use
format()
```

