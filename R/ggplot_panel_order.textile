h1. ggplot panel order

- Categories := R

- Tags := facet, panel, order, sort


To change panel order in a faceted graph, you need to convert the variable used for faceting into a factor. The code below uses prior knowledge of the original sort order.

```r
make_option("--panelOrder", type = "character",
		help = "(Optional) Panel order (comma-separated list of integers to reorder the RefName factor).")

ds$RefName <- factor(ds$RefName)
if (!is.null(opt$panelOrder)) {
	panelOrderArr <- as.integer(unlist(str_split(opt$panelOrder, ",")))
	refNameLevels <- levels(ds$RefName)
	panelN <- length(refNameLevels)
	# Make sure the vector is correct
	if (any(sort(panelOrderArr) != 1:panelN)) {
		stop(sprintf("The panel order array should have each number in 1:%d exactly once.", panelN))
	}
	ds$RefName <- factor(ds$RefName, refNameLevels[panelOrderArr])
}
```

