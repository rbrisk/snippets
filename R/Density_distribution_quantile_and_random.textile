h1. Density, distribution, quantile, and random

- Categories := R

- Tags := binomial, density, distribution, hypergeometric, normal, poisson, quantile, random, uniform

[[More Info|http://ww2.coastal.edu/kingw/statistics/R-tutorials/prob.html]]

Normal

```r
# Density
dnorm(x, mean = 0, sd = 1, log = FALSE)
# Distribution
pnorm(q, mean = 0, sd = 1, lower.tail = TRUE, log.p = FALSE)
# Quantile
qnorm(p, mean = 0, sd = 1, lower.tail = TRUE, log.p = FALSE)
# Random generation
rnorm(n, mean = 0, sd = 1)
```

Poisson

```r
dpois(x, lambda, log = FALSE)
ppois(q, lambda, lower.tail = TRUE, log.p = FALSE)
qpois(p, lambda, lower.tail = TRUE, log.p = FALSE)
rpois(n, lambda)
```

Uniform

```r
dunif(x, min=0, max=1, log = FALSE)
punif(q, min=0, max=1, lower.tail = TRUE, log.p = FALSE)
qunif(p, min=0, max=1, lower.tail = TRUE, log.p = FALSE)
runif(n, min=0, max=1)
```

Binomial

```r
dbinom(x, size, prob, log = FALSE)
pbinom(q, size, prob, lower.tail = TRUE, log.p = FALSE)
qbinom(p, size, prob, lower.tail = TRUE, log.p = FALSE)
rbinom(n, size, prob)
```

Hypergeometric

```r
dhyper(x, m, n, k, log = FALSE)
phyper(q, m, n, k, lower.tail = TRUE, log.p = FALSE)
qhyper(p, m, n, k, lower.tail = TRUE, log.p = FALSE)
rhyper(nn, m, n, k)
```

