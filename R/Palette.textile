h1. Palette

- Categories := R

- Tags := gradient, interpolation, palette, scale

Create a new palette (gradient)

```r
colN <- 100
require(RColorBrewer)
colorRampPalette(brewer.pal(9,"Reds"))(colN)

colorpanel(colN, low="#F45D48", high="#65000F")
```

