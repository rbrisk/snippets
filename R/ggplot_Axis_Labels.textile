h1. ggplot Axis Labels

- Categories := R

- Tags := axis, ggplot, labels, scale

```r
ggplot(df, aes(factor(Probes), Length)) + 
	geom_boxplot() + 
	scale_x_discrete(name="Probes", breaks=seq(0,34,2)) + 
	scale_y_continuous(name="Length", breaks=seq(0,7400,400))
```

Rotate x labels

```r
ggplot(df, aes(Tissue, FPKM, group=Gene, colour=Gene)) +
	geom_line() +
	guides(colour=F) +
	theme(axis.text.x=element_text(angle=90, hjust=1, vjust=0.5))
```

Put x labels in specific order

```r
xLabels <- c("C", "A", "B")
ggplot(df, aes(DMRorientation)) + 
	geom_histogram() +
	scale_x_discrete(limits=xLabels)
```

