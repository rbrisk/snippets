h1. Colorblind friendly palette

- Categories := R

- Tags := Colorblind, color-blind, colour-blind, ggplot2

[[More Info|http://wiki.stdout.org/rcookbook/Graphs/Colors%20%28ggplot2%29/]]

General order: gray, orange, sky-blue, green, yellow, blue, vermilion (reddish), magenta

Starts with gray

```r
cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")
```

Starts with black

```r
cbbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")
```

Starts with orange

```r
cbcPalette <- c("#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")
```

Then it can be applied as

```r
ggplotObj + scale_colour_manual(values=cbPalette)
```

The alternative is to use colour brewer scale

```r
ggplotObj + scale_colour_brewer("Set2", type="div")
ggplotObj + scale_colour_brewer("Dark2", type="div")
ggplotObj + scale_colour_brewer("Accent", type="div")
```

