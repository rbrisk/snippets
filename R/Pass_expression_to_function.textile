h1. Pass expression to function

- Categories := R

- Tags := function, expression, argument

The question was addressed [[here|http://stackoverflow.com/questions/4682709/how-to-write-an-r-function-that-evaluates-an-expression-within-a-data-frame]] and [[here|http://stackoverflow.com/questions/4692231/r-passing-expression-to-an-inner-function]].

Canonical way.

```r
fn <- function(ds, expr) {
	eval(substitute(expr), ds)
}
ds <- data.frame(a = 0:4, b = 5:9)
fn(ds, a * b)
```

If you want all your arguments in a list (whatever that meant).

```r
fn <- function(ds, expr) {
	mf <- match.call()
	eval(mf$expr, envir = dat)
}
```

Passing a string.

```r
eval(parse(text=exprStr), ds)
```

Convert an expression to string to work with the method above.

```r
exprStr = deparse(substitute(expr))
```
