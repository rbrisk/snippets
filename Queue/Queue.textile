h1. Queue

- Categories := Queue

- Tags := GATK, quirks, scala, Queue


h2. Resources

* [[Queue introduction|https://www.broadinstitute.org/gatk/guide/article?id=1307]]
* [[Recommended parallelisation parameters|http://gatkforums.broadinstitute.org/discussion/1975/how-can-i-use-parallelism-to-make-gatk-tools-run-faster] but it does not list HaplotypeCaller
* [[Command line options|http://gatkforums.broadinstitute.org/discussion/1311/qfunction-and-command-line-options]] 


h2. Locale problems

GATK forces @en_US@ locale from the very beginning. However, both GATK and Queue may interact with other software such as GridEngine. Such software may honor system locale settings causing some [[number parsing problems|http://gatkforums.broadinstitute.org/discussion/5435/drmaa-does-not-report-job-completion-status]]. The quick fix is to set the locale as @LC_ALL=C@.

```shell
LC_ALL=C java -jar Queue.jar ...
```


h2. Parallel environment

By default, Qeueu uses @smp_pe@ but it is not defined on fgcz-c-47. The value has to be set to @-jobParaEnv smp@ instead.


h2. Job memory limit

In principle, setting @-memLimit@ would set @-resMemLimit@ to 1.2x and @-resMemReq@ to 1.0x. In reality, you may have to set it at each step explicitly. I am still unclear what exactly happens. The latter two parameters have to be set explicitly. Unfortunately, this may apply to all jobs whether they are parallelised or not.
