h1. Blocks in Ruby

- Categories := Ruby

- Tags := block, review, summary

By convention, curly braces are used for short one-liners @do ... end@ is used for multi-line blocks.

```ruby
(1..4).each { |i| put i }
(1..4).each do |i|
	puts i * 2
	puts "---"
end
```

