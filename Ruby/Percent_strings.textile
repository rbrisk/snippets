h1. Percent strings

- Categories := Ruby

- Tags := percent, shorthand, string

Percent strings. Uppercase letter allows interpolation, lower case disables it. Most non-alphanumeric matching characters can be used for enclosure.

```ruby
interpolation = "test"
%Q(Some #{interpolation}) == "Some test"
%q|No #{interpolation}| == 'No #{interpolation}'
```

Regular expression

```ruby
%r(^regexp$) == /^regexp$/
```

Symbol. Usage???

```ruby
%s(:symbol)
```

Array of strings

```ruby
%w( A B C ) == ["A", "B", "C"]
```

Backtick (capture subshell result)

```ruby
%x
```

