h1. Ligatures in htlatex

- Categories := LaTeX

- Tags := ff, fi, font, htlatex, html, ligature

@htlatex@ replaces ff and fi with weird symbols if a different font is specified with something like

```tex
\usepackage{bera}
\usepackage[T1]{fontenc}
```
Commenting out those lines will prevent ligatures.

[[More Info|http://tug.org/pipermail/texhax/2008-July/010730.html]]
Alternatively, you can use.

```tex
\makeatletter
\@ifundefined{Status}{\gdef\Status{0}}{}
\makeatletter
\ifcase\Status %{0}
	\typeout{HTML output}
\or % Status = 1
	\typeout{PDF output}
	\usepackage{bera}
	\usepackage[T1]{fontenc}
\fi
```

