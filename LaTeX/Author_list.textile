h1. Author list

- Categories := LaTeX

- Tags := author, affiliation

Found [[here|http://tex.stackexchange.com/questions/9594/adding-more-than-one-author-with-different-affiliation#9598]]

```tex
\usepackage{authblk}
\title{THis is a title}
\author[1]{Author A}
\author[2]{Author B}
\author[1]{Author C}
\affil[1]{Department 1}
\affil[2]{Department 2}

\begin{document}
	\maketitle
\end{document}
```
