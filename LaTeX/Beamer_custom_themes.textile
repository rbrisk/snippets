h1. Beamer custom themes

- Categories := LaTeX

- Tags := beamer, color, custom, font, theme

* [[General beamer instructions|http://en.wikibooks.org/wiki/LaTeX/Presentations]]
* [[Short instructions|http://www.r-bloggers.com/create-your-own-beamer-template/]]
* [[Visualisation, single page|http://www.hartwork.org/beamer-theme-matrix/]]
* Visualisation tree
* Beamer appearance cheat sheet (in the docs directory)

@\setbeamercolor@ has accumulative effect, i.e. the following commands are equivalent

```tex
\setbeamercolor{title}{fg=yellow}
\setbeamercolor{title}{bg=blue}
```

and

```tex
\setbeamercolor{title}{fg=yellow,bg=blue}
```

To reset the previous effects, starred version can be used

```tex
\setbeamercolor*{title}{fg=red}
```

