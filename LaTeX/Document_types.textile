h1. Document types

- Categories := LaTeX

- Tags := documentclass, latex, plain

List of document classes can be found [[here|http://texcatalogue.ctan.org/bytopic.html#classes]].

